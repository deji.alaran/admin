class Evaluator:
    
    def __init__(self, thresholds = []):
        self.thresholds = thresholds

    def evaluate(self, telemetry):
        for tele in telemetry:
            thresholds = self.thresholds(tele) if callable(self.thresholds) else self.thresholds
            
            if any(map(lambda i: i == tele["metric"]["name"], thresholds.keys())):
                threshold = thresholds[tele["metric"]["name"]]
                if eval("{}{}".format(tele["metric"]["value"],threshold)):
                    yield EvaluationResults(EvaluationResults.FAILED, "Evaluation failed {} ".format(tele), tele)
                else:
                    yield EvaluationResults(EvaluationResults.SUCCESSFUL, "Evaluated {} successfully".format(tele), tele)
            else:
                yield EvaluationResults(EvaluationResults.SKIPPED, "Skipped {}".format(tele), tele)

class EvaluationResults:
    SUCCESSFUL = "SUCCESS"
    FAILED = "FAILED"
    SKIPPED = "SKIPPED"
    ERROR = "ERROR"

    def __init__(self, status, message, telemetry):
        self.status = status
        self.message = message
        self.telemetry = telemetry