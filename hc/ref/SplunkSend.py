#!/usr/bin/python3
# -*- coding: utf-8 -*-
##########################################################################
# SplunkSend - export metrics from Redis Enterprise to log files to be
# ingested by Splunk
##########################################################################
import argparse
import logging
import os
import platform
import requests
import sys
import time

# local imports
# mainconf is the class for configuring the script
from modules import config as mainconf
# cluster conf defines the cluster object - there is one per cluster in the cluster config file
from modules import clobj as clclass

#######  Globals ######

args = None
logger = None
parser = None
# outfile = None
version = 'A.1.0'

def load_args(parser):
# command line args must be defined here
# the the string following 'help-' appears in the 'usage' message
# when the script is run with '-h' arg
# action-'store_true' sets the var state to "True" when the var does not
# pass in a value
    parser.add_argument("-i", help="ini file - default 'Send2Splunk.ini'")
    parser.add_argument("-c", help="config directory - top level DC config")
    parser.add_argument("-v", help="verbose mode", action="store_true")
    parser.add_argument("-d", help="debug mode", action="store_true")
    parser.add_argument("-n", help="no-op - dry run mode, no metrics sent", action="store_true")
    parser.add_argument("-x", help="Open log with O_APPEND - needed by logrotate", action="store_true")
    return parser

def loadexcludes(excludefile):
    # validate input if the pattern doesn't match the 'if' it will be
    # silently dropped  - this is just loading the config
    excludelist = []
    with open(excludefile, 'r') as excl:
        for line in excl:
            line = line.rstrip()  # strip newlines
            if line.startswith('#'):
                continue
            if line.startswith('^'):
                excludelist.append(line)
            elif line.endswith('$'):
                excludelist.append(line)
            else:
                excludelist.append(line)

    return excludelist

def loadClusters(runconf):
    # load cluster config info from config file
    clusterlist = []
    with open(os.path.join(runconf.confdir, runconf.clusters)) as clconf:
        header = clconf.readline()
        clkeys = header.split(',')
        for clstr in clconf:
            if clstr.startswith('#'):
                continue
            clvals = clstr.split(',')
            if clvals[0]:
                newobj = clclass.Cluster(clvals[0])
            else:
                if runconf.loglevel:
                    logging.error("cluster not found in config record:")
                    logging.error("{}".format(clstr))
                quit()
            for i in range(len(clkeys)):
                if clvals[i]:
                    newobj.setvar(clkeys[i].lower().rstrip(), clvals[i].rstrip())
            clusterlist.append(newobj)
            del newobj

    return clusterlist


def pollcluster(cluster, runconf):
    # scrape the Prometheus endpoint
    prometheus = "https://" + cluster.host + ":" + cluster.port
    try:
        # WARNING - 'verify=False' disable cert checking so we can test with self-signed certs
        # not for production use
        thisPage = requests.get(prometheus, auth=(cluster.user, cluster.passwd), verify=False)
    except requests.exceptions.RequestException as e:
        print(e)
#        exit(-2)
        return ""
    if thisPage.status_code != 200:
        if runconf.loglevel:
            logging.error("ERROR: http status: {}".format(thisPage.status_code))
            logging.error("  url: {}".format(prometheus))
#        exit(-1)
        return ""

    return thisPage.text

def formatstat(stat, ts):
    # there is one space in the rec - it precedes the metric value
    scratch, mvalue = stat.split(' ')
    mvalue = mvalue.rstrip()  # strip the '\n'

    # metric name precedes the tags - which are delimited by '{}'
    # so we split the scratch string at '{'
    # the left part is the metric name -  data within '{}' is
    # a comma delimited set of tags
    # some metrics have no tags, hence no '{' and the need for the 'if'
    ptr = scratch.find('{')
    if ptr > -1:
        mname = scratch[0:ptr]
        jsonstring =scratch[ptr:]
        # knock off the {}
        jsonstring = jsonstring[1:-1]
        tagarray = jsonstring.split(',')
        statstring = '{"time": "' + ts + '","metric": {"' + mname + '": "' + mvalue + '"}'
        for tag in tagarray:
            tkey, tval = tag.split('=')

            tagstring = ',"' + tkey + '": ' + tval
            statstring = statstring + tagstring
        statstring = statstring + "}"
    else:
        # we already split the metric at ' ' - if there are no tags 'scratch' is the metric name
        statstring = '{"time": "' + ts + '","metric": {"' + scratch + '": "' + mvalue + '"}}'

    return statstring + '\n'

def checkmatch(metric, excl):
    # metrics with tags have a brace - otherwise the split is on ' '
    # in order to get the metric name
    # Return 'True' on first match in exclude list
    ptr = metric.find('{')
    if ptr > -1:
        name, scratch = metric.split('{')
    else:
        ptr = metric.find(' ')
        if ptr == -1:
            print("DEBUG: metric: {}".format(metric))
        else:
            name, scratch = metric.split(' ')
    for rule in excl:
        if rule.startswith('^'):
            if name.startswith(rule[1:]):  # drop the '^'
                return True
        elif rule.endswith('$'):
            if name.endswith(rule[:-1]):   # drop the '$'
                return True
        ptr = name.find(rule)
        if ptr > -1:  # we found rule in name
            return True

    return False

def logstats(prom, mlog, excludelist, args):
    # create a timestamp for this batch of metrics
    timestamp = time.strftime("%Y%m%d_%H%M%S")
    stats = prom.split('\n')
    for astat in stats:
        if astat.startswith('#'):
            continue
        # the last record is null
        if len(astat) < 1:
            continue
        # check exclude list - if we get a match we drop this one
        if excludelist:
            SkipThis = checkmatch(astat, excludelist)
            if SkipThis:
                # print("DEBUG: skipping: {}".format(astat))
                continue
        splunkstat = formatstat(astat, timestamp)
        splunkbytes = str.encode(splunkstat)
        # if this scripts is being used in conjunction with logrotate we need to
        # use os.write() in with O_APPEND on the file open to be compatible with
        # logrotate's use of 'copytruncate'
        if args.x:
            mlog.write('{}\n'.format(splunkstat))
        else:
            writecount = os.write(mlog, splunkbytes)

def main():
    # global logger

    localtime = time.asctime()
    thisPlatform = platform.platform()
    parser = argparse.ArgumentParser()
    parser = load_args(parser)
    args = parser.parse_args()
    if args.i:
        run_dir = args.i
    else:
        run_dir = os.getcwd()
    runconf = mainconf.Config()
    runconf.load(os.path.join(run_dir, "Send2Splunk.ini"))
    if runconf.loglevel != 'none':
        # 'thislog' is the runtime log for this script
        thislog = "SplunkSend_{}.log".format(time.strftime("%Y%m%d_%H%M%S"))
        #logname = os.path.join(current_dir, runconf.logdir, thislog)
        logname = os.path.join(runconf.logdir, thislog)
        logging.basicConfig(filename=logname,
                            format='%(asctime)s %(message)s',
                            level=runconf.loglevel)

        logging.info("INFO: logdir: {}  loglevel: {}".format(runconf.logdir, runconf.loglevel))
        logging.debug('Starting run: Script file: %s ', sys.argv[0])
        logging.debug('Command line args: {}'.format(sys.argv[1:]))
        logging.debug('Script version: {}'.format(version))
        logging.debug("runtime args: {}".format(sys.argv[1:]))
        logging.info("start time : " + localtime)
        logging.info("Platform : {}".format(thisPlatform))
        logging.info("Python   : {}".format(platform.python_version()))
        logging.info("Poll interval : {} seconds".format(runconf.pollinterval))

    clusters = loadClusters(runconf)
    mlog = runconf.metricslog

    #    mlog = mlog + '-' + time.strftime("%Y%m%d_%H%M%S")
    # metricsog is the formatted metrics output
    #metricslog = open(mlog, 'w')

    # WARNING - 'verify=False' disables cert checking on request.get() so we can test with self-signed certs
    # not for production use - the following suppresses the warning from each use of this call
    requests.packages.urllib3.disable_warnings()

    if runconf.loglevel:
        logging.info("The following clusters will be monitored by this process:")
        for cluster in clusters:
            logging.info("Cluster name: {}  host: {}  port: {} ".format(cluster.name, cluster.host, cluster.port))

    sleeptime = int(runconf.pollinterval)

    # metricsog is the formatted metrics output
    # if this scripts is being used in conjunction with logrotate we need to
    # use os.write() in with O_APPEND on the file open to be compatible with
    # logrotate's use of 'copytruncate'
    if args.x:
        metricslog = open(mlog,'w')
    else:
        metricslog = os.open(mlog, os.O_RDWR|os.O_APPEND|os.O_CREAT)


    # WARNING - 'verify=False' disables cert checking on request.get() so we can test with self-signed certs
    # not for production use - the following suppresses the warning from each use of this call
    requests.packages.urllib3.disable_warnings()

    sleeptime = int(runconf.pollinterval)

    fatalerror = False

    # excludelist contains all patterns used to skip metrics - a null list will cause the
    # check to be skipped for all metrics
    excludelist = []
    if runconf.excludes:
        if runconf.loglevel:
            logging.info("Metrics exclusion patterns were configured - pattern list follows")
        excludelist = loadexcludes(runconf.excludes)
        for line in excludelist:  # if a null list is returned nothing is logged
            # if were logging at INFO or better, log the patterns we loaded
            if runconf.loglevel:
                logging.info("Active exclude pattern: {}".format(line))
    #  DEBUG counter
    x = 0
    while not fatalerror:
        for cluster in clusters:
            promstats = pollcluster(cluster, runconf)
            if args.n:   # dry run, no logging
                continue
            if promstats:
                logstats(promstats, metricslog, excludelist, args)
        time.sleep(sleeptime)

if __name__ == '__main__':
    main()
logging.info('End run')
sys.exit()





