class Cluster:

    def __init__(self, name):
        self.name = name
        self.port = '9443'
        self.host = 'localhost'
        self.passwd = 'xxxx'
        self.desc = "Redis Enterprise Cluster"

    def name(self):
        return self.name

    def port(self):
        return self.port

    def host(self):
        return self.host

    def passwd(self):
        return self.passwd

    def pollcount(self):
        return pollcount

    def bumpPollCount(self):
        self.pollcount += 1

    def unpoll(self):
        self.pollcount = -5

    def setvar(self, key, value):
        import base64
        if key == 'clname':
            pass
        elif key == 'clport':
            self.port = value
        elif key == 'clhost':
            self.host = value
        elif key == 'cluser':
            self.user = value
        elif key == 'clpass':
            self.passwd = value
            if value:
                self.passwd = base64.b64decode(value)
                self.passwd = self.passwd.rstrip()
        elif key == 'cldesc':
            self.desc = value
        else:
            print("DEBUG: ERROR: setvar() received unknown key: {}".format(key))
            logging.error("Method setvar: received unknown key: {}".format(key))
        return

