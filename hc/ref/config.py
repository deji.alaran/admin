class Config:
    def __init__(self):
        self.confvers = ''
        self.confdir = ''
        self.logdir = 'logs'
        self.loglevel = 'INFO'
        self.pollinterval = ''
        self.clusters = "clusters.csv"
        self.metricslog = "metrics.log"
        self.splunkHost = ''
        self.splunkSocket = ''
        self.splunkUDP = ''
        return

    def load(self, infile):
        with open(infile) as cfile:
            for line in cfile:
                if len(line) == 1:        # blank line
                    continue
                if line.startswith('#'):  # comment line
                    continue
                line = line.rstrip()
                key, value = line.split(': ')
                if key == "confversion":
                    self.confvers = value
                elif key == "confdir":
                    self.confdir = value
                elif key == 'logdir':
                    self.logdir = value
                elif key == 'loglevel':
                    self.loglevel = value
                elif key == 'pollinterval':
                    self.pollinterval = value
                elif key == 'clusters':
                    self.clusters = value
                elif key == 'mlog':
                    self.metricslog = value
                elif key == 'excludes':
                    self.excludes = value
                else:
                    print("ERROR: unknown config value: {} \n".format(line))

    def logdir(self):
        return self.logdir

    def loglevel(self):
        return self.loglevel

