from rs import Cluster
import time
import json

class TelemetryPoller:
    def __init__(self, fqdn_host="localhost", telemetry_port=8070, verify=True):
        self.fqdn_host=fqdn_host
        self.telemetry_port=telemetry_port
        self.verify=verify

    def poll(self):
        c = Cluster.Cluster(fqdn_host=self.fqdn_host, telemetry_port=self.telemetry_port, verify=self.verify)
        telemetry_data = c.telemetry()
        for tele in self.parse_telemetry(telemetry_data):
            yield tele

    def parse_telemetry(self, prom):
        stats = prom.split('\n')
        print("logging stats")
        timestamp = time.strftime("%Y%m%d_%H%M%S")
        for astat in stats:
            if astat.startswith('#'):
                continue
            # the last record is null
            if len(astat) < 1:
                continue
            yield self.formatstat(astat, timestamp)

    @staticmethod
    def formatstat(stat, ts):
        # there is one space in the rec - it precedes the metric value
        scratch, mvalue = stat.split(' ')
        mvalue = mvalue.rstrip()  # strip the '\n'

        # metric name precedes the tags - which are delimited by '{}'
        # so we split the scratch string at '{'
        # the left part is the metric name -  data within '{}' is
        # a comma delimited set of tags
        # some metrics have no tags, hence no '{' and the need for the 'if'
        ptr = scratch.find('{')
        if ptr > -1:
            mname = scratch[0:ptr]
            jsonstring =scratch[ptr:]
            # knock off the {}
            jsonstring = jsonstring[1:-1]
            tagarray = jsonstring.split(',')
            statstring = '{"time": "' + ts + '","metric": {"name":"' + mname + '", "value": "' + mvalue + '"}'
            for tag in tagarray:
                tkey, tval = tag.split('=')

                tagstring = ',"' + tkey + '": ' + tval
                statstring = statstring + tagstring
            statstring = statstring + "}"
        else:
            # we already split the metric at ' ' - if there are no tags 'scratch' is the metric name
            statstring = '{"time": "' + ts + '","metric": {"name":"' + scratch + '", "value": "' + mvalue + '"}}'
        return json.loads(statstring)