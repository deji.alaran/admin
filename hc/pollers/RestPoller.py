from rs import Cluster
import time
import json

class RestPoller:
    def __init__(self, fqdn_host, cluster_username, cluster_password, api_port=9443, verify=True, accept_db=lambda s : True):
        self.fqdn_host=fqdn_host
        self.cluster_username=cluster_username
        self.cluster_password=cluster_password
        self.api_port=api_port
        self.verify=verify
        self.accept_db = accept_db

    def db_component(poller):
        def allow(*args, **kwargs):
            if 'poll_db' in kwargs and not kwargs['poll_db']:
                return ()
            return poller(*args, **kwargs)
        return allow

    def node_component(poller):
        def allow(*args, **kwargs):
            if 'poll_node' in kwargs and not kwargs['poll_node']:
                return ()
            return poller(*args, **kwargs)
        return allow

    def cluster_component(poller):
        def allow(*args, **kwargs):
            if 'poll_cluster' in kwargs and not kwargs['poll_cluster']:
                return ()
            return poller(*args, **kwargs)
        return allow

    def setup_category(poller):
        def allow(*args, **kwargs):
            if 'poll_setup' in kwargs and not kwargs['poll_setup']:
                return ()
            return poller(*args, **kwargs)
        return allow

    def monitor_category(poller):
        def allow(*args, **kwargs):
            if 'poll_monitor' in kwargs and not kwargs['poll_monitor']:
                return ()
            return poller(*args, **kwargs)
        return allow

    def health_category(poller):
        def allow(*args, **kwargs):
            if 'poll_health' in kwargs and not kwargs['poll_health']:
                return ()
            return poller(*args, **kwargs)
        return allow

    def poll(self, *args, **kwargs):
        results = self.__poll_slowlog(*args, **kwargs)
        for result in results:
            yield result

        results = self.__poll_nodes_with_master_shards_but_no_endpoint(*args, **kwargs)
        for result in results:
            yield result

        results = self.__poll_cluster_topology(*args, **kwargs)
        for result in results:
            yield result        

    @db_component
    @health_category
    def __poll_slowlog(self, *args, **kwargs):
        c = self.__get_conn()
        dbs = self.__fetch_databases()
        for db in dbs:
            sll = db.slowlog_len()
            yield self.formatstat('slowlog_count', sll, {"cluster" : self.fqdn_host, "db": db.name()})
            if sll > 0:
                sls = db.slowlog(count=25)
                for sl in sls:
                    source = {"cluster": self.fqdn_host, "db": db.name(), "slowlog_id": sl["id"]}
                    yield self.formatstat('slowlog_duration', sl['duration'], source)
                    yield self.formatstat('slowlog_command', sl['command'], source)

    @node_component
    @monitor_category
    def __poll_nodes_with_master_shards_but_no_endpoint(self, *args, **kwargs):
        flat_map = lambda f, xs: [y for ys in xs for y in f(ys)]

        c = self.__get_conn()
        
        nodes = dict(map(lambda n: [ n.addr(), n ], list(c.nodes())))

        dbs = list(self.__fetch_databases())

        master_shards = list(filter(lambda shard: shard.isMaster(), flat_map(lambda s: s, list(map(lambda d: d.shards(), dbs)))))

        master_shards_to_nodes = list(map(lambda s: s.node_uid(), master_shards))

        endpoints_to_nodes = set(map(lambda addr: nodes[addr].uid(), flat_map(lambda endpoint: endpoint.addresses(), flat_map(lambda d: d.endpoints(), dbs))))
        
        nodes_with_master_shards_but_no_endpoint = filter(lambda uid: uid not in endpoints_to_nodes, master_shards_to_nodes)

        source = {"cluster": self.fqdn_host}

        yield self.formatstat('nodes_with_master_shards_but_no_endpoint', len(list(nodes_with_master_shards_but_no_endpoint)), source)

    @cluster_component
    @monitor_category
    def __poll_cluster_topology(self, *args, **kwargs):
        c = self.__get_conn()

        currenttopology = dict(map(lambda t: (t['name'], t), list(kwargs['current-topology']))) if 'current-topology' in kwargs else {}
        bdbs = list(c.database_topology())
        newtopology = dict(map(lambda t: (t['name'], t), bdbs))
        s_currenttopology = set(currenttopology)
        s_newtopology = set(newtopology)

        source = {"cluster": self.fqdn_host}

        for added in s_newtopology - s_currenttopology:
            source['db'] = added
            yield self.formatstat('cluster_topology_db_added', 1, source)

        for removed in s_currenttopology - s_newtopology:
            source['db'] = removed
            yield self.formatstat('cluster_topology_db_removed', 1, source)

        for both in s_newtopology.intersection(s_currenttopology):
            nms = set(newtopology[both]['main-shards-nodes']) if 'main-shards-nodes' in newtopology[both] else set()
            nrs = set(newtopology[both]['replica-shards-nodes']) if 'replica-shards-nodes' in newtopology[both] else set()
            cms = set(currenttopology[both]['main-shards-nodes']) if 'main-shards-nodes' in currenttopology[both] else set()
            crs = set(currenttopology[both]['replica-shards-nodes']) if 'replica-shards-nodes' in currenttopology[both] else set()

            if nms != cms or nrs != crs:
                source['db'] = both
                yield self.formatstat('cluster_topology_db_shards_moved', 1, source)

    def __fetch_databases(self):
        conn =self.__get_conn()
        dbs =conn.databases()
        for db in dbs:
            if self.accept_db(db):
                yield db

    def __get_conn(self):
        if not hasattr(self, 'cluster_conn'):
            self.cluster_conn = Cluster.Cluster(fqdn_host=self.fqdn_host, cluster_username=self.cluster_username, cluster_password=self.cluster_password, api_port=self.api_port, verify=self.verify)
        return self.cluster_conn
        

    @staticmethod
    def formatstat(name, value, source):
        ts = time.strftime("%Y%m%d_%H%M%S")
        storevalue = value if isinstance(value, int) else '"%s"' % value
        statstring = '{"time": "%s", "metric": {"name":"%s", "value": %s}}' % (ts, name, storevalue)
        response =json.loads(statstring)
        response["source"] = source
        return response