class SplunkFormatter:
    def format(evaluations):
        for evaluation in evaluations:
            obj = evaluation.telemetry
            obj["status"] = evaluation.status
            obj["metric"] = {
                evaluation.telemetry["metric"]["name"]: evaluation.telemetry["metric"]["value"]
            }
            yield obj, evaluation
