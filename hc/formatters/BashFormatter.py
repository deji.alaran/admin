from hc.evaluators import Evaluator

class BashFormatter:
    def format(evaluations):
        for evaluation in evaluations:
            if evaluation.status == Evaluator.EvaluationResults.SUCCESSFUL:
                to_print = [Color.green('[+]'), evaluation.message, Color.green('[SUCCEEDED]')]    
            elif evaluation.status == Evaluator.EvaluationResults.SKIPPED:
                to_print = [Color.yellow('[~]'), evaluation.message, Color.yellow('[SKIPPED]')]   
            else:
                to_print = [Color.red('[-]'), evaluation.message, Color.red('[FAILED]')]
            yield '{} {} {}'.format(*to_print), evaluation








#     doc = (_result[2] if len(_result) == 3 else _func.__doc__).split('\n')[0]
#     remedy = None
#     if _result[0] == '':
#         to_print = ['[ ]', doc, '[SKIPPED]']
#     elif _result[0] is True:
#         to_print = [Color.green('[+]'), doc, Color.green('[SUCCEEDED]')]
#     elif _result[0] is False:
#         to_print = [Color.red('[-]'), doc, Color.red('[FAILED]')]
#         doc = (_result[2] if len(_result) == 3 else _func.__doc__)
#         remedy = re.findall(r'Remedy: (.*)', doc, re.MULTILINE)[0]
#     elif _result[0] is None:
#         to_print = [Color.yellow('[~]'), doc, Color.yellow('[NO RESULT]')]
#     elif _result[0] is Exception:
#         to_print = [Color.magenta('[*]'), doc, Color.magenta('[ERROR]')]
#     else:
#         raise NotImplementedError()

#     to_print.append(', '.join([str(k) + ': ' + str(v) for k, v in _result[1].items()]))
#     if remedy:
#         to_print.append(' '.join([Color.cyan('Remedy:'), remedy]))
#         print('{} {} {} {} {}'.format(*to_print))
#     else:
#         print('{} {} {} {}'.format(*to_print))


class Color(object):
    """
    Color class.
    Use ASCII escape codes to colorize terminal output.
    """

    @staticmethod
    def black(_text):
        return '\033[0;30m' + _text + '\033[0m'

    @staticmethod
    def red(_text):
        return '\033[0;31m' + _text + '\033[0m'

    @staticmethod
    def green(_text):
        return '\033[0;32m' + _text + '\033[0m'

    @staticmethod
    def yellow(_text):
        return '\033[0;33m' + _text + '\033[0m'

    @staticmethod
    def blue(_text):
        return '\033[0;34m' + _text + '\033[0m'

    @staticmethod
    def magenta(_text):
        return '\033[0;35m' + _text + '\033[0m'

    @staticmethod
    def cyan(_text):
        return '\033[0;36m' + _text + '\033[0m'

    @staticmethod
    def white(_text):
        return '\033[0;37m' + _text + '\033[0m'