from distutils.core import setup
from setuptools import find_packages

setup(
    name='re-tools',
    author='Deji',
    author_email='deji.alaran@redislabs.com',
    version='0.1.5',
    packages=find_packages(exclude=["tests"]),
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description=open('README.txt').read(),
)
