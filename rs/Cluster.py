
import requests
import base64
import json
from rs import Database, Node


class Cluster:
    def __init__(self, fqdn_host="localhost", api_port=9443, cluster_username=None,  cluster_password=None, telemetry_port=8070, verify=True):
        self.fqdn_host = fqdn_host
        self.api_port = api_port
        self.telemetry_port = telemetry_port
        self.verify = verify
        self.cluster_username = cluster_username
        self.cluster_password = cluster_password

    def telemetry(self):
        url = "https://%s:%s/metrics" % (self.fqdn_host, self.telemetry_port)
        print(url)
        try:
            response = requests.get(url, verify=self.verify)
            return response.text
        except requests.exceptions.RequestException as e:
            print(e)
            # TODO: handle exception properly
            # return []

    def nodes(self):
        credentials = "%s:%s" % (self.cluster_username, self.cluster_password)
        encoded_credentials = base64.b64encode(credentials.encode('ascii'))
        header = {
            'Authorization': 'Basic %s' % encoded_credentials.decode("ascii")}
        status, nodes = self.fetch_url(
            "https://%s:%s/v1/nodes" % (self.fqdn_host, self.api_port), header)

        for node in nodes:
            yield Node.Node(node, fqdn_host=self.fqdn_host, api_port=self.api_port, cluster_username=self.cluster_username, cluster_password=self.cluster_password, verify=self.verify)

    def databases(self):
        credentials = "%s:%s" % (self.cluster_username, self.cluster_password)
        encoded_credentials = base64.b64encode(credentials.encode('ascii'))
        header = {
            'Authorization': 'Basic %s' % encoded_credentials.decode("ascii")}
        status, dbs = self.fetch_url(
            "https://%s:%s/v1/bdbs" % (self.fqdn_host, self.api_port), header)

        for db in dbs:
            yield Database.Database(db, fqdn_host=self.fqdn_host, api_port=self.api_port, cluster_username=self.cluster_username, cluster_password=self.cluster_password, verify=self.verify)

    def database_topology(self):
        dbs = self.databases()

        for db in dbs:
            shards = db.shards()
            dbname = db.descriptor['name']

            mainshards = set()
            replicashards = set()

            for shard in shards:
                if shard.isMaster():
                    mainshards.add(shard.node_uid())
                else:
                    replicashards.add(shard.node_uid())
            
            yield {
                'name' : dbname,
                'main-shards-nodes': list(mainshards),
                'replica-shards-nodes': list(replicashards)
            }

    def fetch_url(self, url, header):
        print(url)
        try:
            response = requests.get(url, headers=header, verify=self.verify)
        except requests.exceptions.RequestException as e:
            print(e)
            return (500, e)

        return (response.status_code, json.loads(response.content.decode('utf-8')) if len(response.content) > 0 else None)
