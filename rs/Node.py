import requests
import base64
import json

class Node:
    def __init__(self, descriptor, fqdn_host="localhost", api_port=9443, cluster_username=None,  cluster_password=None, verify=True):
        self.descriptor=descriptor
        self.fqdn_host = fqdn_host
        self.api_port=api_port
        self.verify=verify
        self.cluster_username=cluster_username
        self.cluster_password=cluster_password

    def uid(self):
        return self.descriptor["uid"]

    def addr(self):
        return self.descriptor["addr"]
