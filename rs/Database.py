import requests
import base64
import json

class Database:
    def __init__(self, descriptor, fqdn_host="localhost", api_port=9443, cluster_username=None,  cluster_password=None, verify=True):
        self.descriptor=descriptor
        self.fqdn_host = fqdn_host
        self.api_port=api_port
        self.verify=verify
        self.cluster_username=cluster_username
        self.cluster_password=cluster_password

    def uid(self):
        return self.descriptor["uid"]

    def name(self):
        return self.descriptor["name"]

    def slowlog(self, count=10):
        sls = self.execute_command("slowlog get %s" % count)
        for sl in sls[1]['response']:
            yield {'id': sl[0], 'duration': sl[2], 'command': " ".join(sl[4])}

    def slowlog_len(self):
        sls = self.execute_command("slowlog len")
        return sls[1]['response']

    def slowlogconfig_get(self):
        return (self.execute_command("config get slowlog-log-slower-than")[1]["response"][1], self.execute_command("config get slowlog-max-len")[1]["response"][1])

    def slowlogslowerthan_set(self, slower_than:int):
        self.execute_command("config set slowlog-log-slower-than %s" % (slower_than))

    def slowlogsmaxlen_set(self, length:int):
        self.execute_command("config set slowlog-max-len %s" % length)

    def shards(self):
        for shard in self.descriptor["shard_list"]:
            credentials = "%s:%s" % (self.cluster_username, self.cluster_password)
            encoded_credentials = base64.b64encode(credentials.encode('ascii'))
            header = {
                'Authorization': 'Basic %s' % encoded_credentials.decode("ascii")}
            status, shard = fetch_url("https://%s:%s/v1/shards/%s" % (self.fqdn_host, self.api_port, shard), header, self.verify)
            yield Shard(shard)

    def endpoints(self):
        for ep in self.descriptor["endpoints"]:
            yield Endpoint(ep)

    def execute_command(self, command):
        credentials = "%s:%s" % (self.cluster_username, self.cluster_password)
        encoded_credentials = base64.b64encode(credentials.encode('ascii'))
        header = {
            'Authorization': 'Basic %s' % encoded_credentials.decode("ascii")}
        url = "https://%s:%s/v1/bdbs/%s/command" % (self.fqdn_host, self.api_port, self.descriptor['uid'])
        try:
            request = {"command": command}
            response = requests.post(url, headers=header, json=request, verify=self.verify)
        except requests.exceptions.RequestException as e:
            print(e)
            return (500, e)
        return (response.status_code, json.loads(response.content.decode('utf-8')) if response.content else None)

def fetch_url(url, header, verify=True):
    print(url)
    try:
        response = requests.get(url, headers=header, verify=verify)
    except requests.exceptions.RequestException as e:
        print(e)
        return (500, e)
    return (response.status_code, json.loads(response.content.decode('utf-8')) if response.content else None)

class Shard:
    def __init__(self, descriptor):
        self.descriptor=descriptor

    def isMaster(self):
        return self.descriptor["role"] == "master"

    def node_uid(self):
        return int(self.descriptor["node_uid"])

    def uid(self):
        return self.descriptor["uid"]
        
class Endpoint:
    def __init__(self, descriptor):
        self.descriptor=descriptor

    def addresses(self):
        return self.descriptor["addr"]
