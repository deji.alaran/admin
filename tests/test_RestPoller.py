from hc.pollers import RestPoller
from rs import Cluster, Database
import json
import pytest
from .data.Slowlog import slowlog
from .data.nodes import nodes
from .data.bdbs import bdbs
from .data.shards import shards
from .data.bdbs_1 import bdbs_1
from .data.shards_1 import shards_1

CLUSTER_HOST = "cluster_host"
CLUSTER_USER = "username"
CLUSTER_PASS = "password"

@pytest.mark.parametrize("test_input,expected", [
   (lambda db: db.name() == 'db-1' or db.uid() == '1', 1),
   (lambda db: db.name() == 'db-1' or db.uid() == '2', 2)])
def test_fetch_databases(mocker, test_input, expected):
    mocker.patch.object(Cluster.Cluster, 'fetch_url', return_value=(
        200, [{'name': 'db-1', 'uid': "1"}, {'name': 'db-2', 'uid': "2"}, {'name': 'db-3', 'uid': "3"}]))

    r = RestPoller.RestPoller(
        CLUSTER_HOST, CLUSTER_USER, CLUSTER_PASS, verify=False, accept_db=test_input)
    
    dbs = list(r._RestPoller__fetch_databases())

    assert len(dbs) == expected

def test_fetch_databases_no_accept_db_param(mocker):
    mocker.patch.object(Cluster.Cluster, 'fetch_url', return_value=(
        200, [{'name': 'db-1', 'uid': "1"}, {'name': 'db-2', 'uid': "2"}, {'name': 'db-3', 'uid': "3"}]))

    r = RestPoller.RestPoller(
        CLUSTER_HOST, CLUSTER_USER, CLUSTER_PASS, verify=False)
    
    dbs = list(r._RestPoller__fetch_databases())

    assert len(dbs) == 3

def test_poll_slowlog(mocker):
    mocker.patch.object(Cluster.Cluster, 'fetch_url', return_value=(
        200, [{'name': 'db-1', 'uid': "1"}]))
    mocker.patch.object(Database.Database, 'execute_command', side_effect=[(200, {'response':100}), slowlog])

    r = RestPoller.RestPoller(
        CLUSTER_HOST, CLUSTER_USER, CLUSTER_PASS, verify=False)
    
    sl = list(r._RestPoller__poll_slowlog())

    assert(sl[0]['metric']['name'] == 'slowlog_count')
    assert(sl[0]['metric']['value'] == 100)
    assert(len(sl) == 113)

@pytest.mark.parametrize("test_input,expected", [
   ("172.16.22.21", 0),
   ("172.16.22.22", 1)])
def test_poll_nodes_with_master_shards_but_no_endpoint(mocker, test_input, expected):
    bdbs[0]["endpoints"][0]["addr"][0] = test_input
    mocker.patch.object(Cluster.Cluster, 'fetch_url', side_effect=[(
        200, nodes),(200, bdbs)])
    mocker.patch.object(Database, 'fetch_url', side_effect=[(200, shards[0]),(200, shards[1])])

    r = RestPoller.RestPoller(
        CLUSTER_HOST, CLUSTER_USER, CLUSTER_PASS, api_port=80, verify=False)
    
    nl = list(r._RestPoller__poll_nodes_with_master_shards_but_no_endpoint())
    assert(nl[0]['metric']['name'] == 'nodes_with_master_shards_but_no_endpoint')
    assert(nl[0]['metric']['value'] == expected)
    assert(len(nl) == 1)

@pytest.mark.parametrize("test_input,expected", [
    #([poll_db,poll_node,poll_health,poll_monitor])
   ([True,True,True,True], 114),
   ([False,True,True,True], 1),
   ([False,False,True,True], 0),
   ([True,False,True,True], 113),
   ([True,True,False,True], 1),
   ([False,True,False,True], 1),
   ([False,False,False,True], 0),
   ([True,False,False,True], 0),

   ([True,True,True,False], 113),
   ([False,True,True,False], 0),
   ([False,False,True,False], 0),
   ([True,False,True,False], 113),
   ([True,True,False,False], 0),
   ([False,True,False,False], 0),
   ([False,False,False,False], 0),
   ([True,False,False,False], 0)

])
def test_poll(mocker, test_input, expected):
    cluster_se = []

    #more generic test needed that can read input file and determine various combinations and test them

    fetch_slowlog = test_input[0] and test_input[2] 
    if fetch_slowlog:
        cluster_se.append((200, bdbs))
        mocker.patch.object(Database.Database, 'execute_command', side_effect=[(200, {'response':100}), slowlog])

    if test_input[1] and test_input[3]:
        cluster_se.append((200, nodes))
        cluster_se.append((200, bdbs))
        mocker.patch.object(Database, 'fetch_url', side_effect=[(200, shards_1[0]),(200, shards_1[1])])

    mocker.patch.object(Cluster.Cluster, 'fetch_url', side_effect=cluster_se)

    r = RestPoller.RestPoller(
        CLUSTER_HOST, CLUSTER_USER, CLUSTER_PASS, verify=False)
    
    sl = list(r.poll(poll_db=test_input[0],poll_node=test_input[1],poll_cluster=False,poll_health=test_input[2],poll_monitor=test_input[3]))
    assert(len(sl) == expected)


bdbs_shards = [(200, shards[0]),(200, shards[0])]
bdbs_1_shards= [(200, shards_1[0]),(200, shards_1[1]),(200, shards_1[2]),(200, shards_1[3])]

@pytest.mark.parametrize("databases,currenttopology,shards,expected_metric_name", [
    #add new db, db-2.  no changes to db-1 topology
    ([bdbs[0], bdbs_1[0]], [{'name': 'db-1','main-shards-nodes': [1]}], bdbs_shards + bdbs_1_shards, 'cluster_topology_db_added'),
    #add new db, db-2.  changes to db-1 topology
    ([bdbs[0], bdbs_1[0]], [{'name': 'db-1','main-shards-nodes': [1],'replica-shards-nodes':[2,3]}], bdbs_shards + bdbs_1_shards, ['cluster_topology_db_added','cluster_topology_db_shards_moved']),
    #remove db-2.  no changes to db-1 topology
    ([bdbs[0]], [{'name': 'db-1','main-shards-nodes': [1]},{'name': 'db-2'}], bdbs_shards, 'cluster_topology_db_removed'),
    #remove db-2.  changes to db-1 topology
    ([bdbs[0]], [{'name': 'db-1','main-shards-nodes': [2]},{'name': 'db-2'}], bdbs_shards, ['cluster_topology_db_removed','cluster_topology_db_shards_moved']),
    #change db-1 topology
    ([bdbs[0]], [{'name': 'db-1', 'main-shards-nodes': [2]}], bdbs_shards, 'cluster_topology_db_shards_moved'),
    #no changes to topology
    ([bdbs[0]], [{'name': 'db-1', 'main-shards-nodes': [1]}], bdbs_shards, None)
])
def test_topology(mocker, databases, currenttopology, shards, expected_metric_name):
    mocker.patch.object(Cluster.Cluster, 'fetch_url', return_value=(
        200, databases))
    
    mocker.patch.object(Database, 'fetch_url', side_effect=shards)

    r = RestPoller.RestPoller(
        CLUSTER_HOST, CLUSTER_USER, CLUSTER_PASS, verify=False)
    
    args = {
        "current-topology" : currenttopology
    }

    tele = list(r._RestPoller__poll_cluster_topology(**args))
    telenames = list(map(lambda x: x['metric']['name'], tele))

    if expected_metric_name is not None:
        assert(telenames == expected_metric_name if type(expected_metric_name) is list else telenames[0] == expected_metric_name )
        assert(tele[0]['metric']['value'] == 1)
        assert(len(tele) == len(expected_metric_name) if type(expected_metric_name) is list else 1)
    else:
        assert(len(tele) == 0)