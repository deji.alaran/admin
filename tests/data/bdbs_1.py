bdbs_1=[
  {
    "gradual_src_mode": "disabled",
    "group_uid": 0,
    "memory_size": 2147483648,
    "last_changed_time": "2020-07-14T21:55:04Z",
    "created_time": "2020-07-14T21:38:50Z",
    "skip_import_analyze": "disabled",
    "rack_aware": False,
    "redis_version": "6.0",
    "oss_sharding": False,
    "shard_list": [
      1,
      2,
      3,
      4
    ],
    "authentication_ssl_client_certs": [],
    "backup_progress": 0.0,
    "import_status": "",
    "hash_slots_policy": "16k",
    "dataset_import_sources": [],
    "roles_permissions": [],
    "replication": True,
    "authentication_admin_pass": "E9FK10ZvTxmbjgaMBAH8GXotyGyJcoA2xY7vdkhRze1Z1SWk",
    "default_user": True,
    "name": "db-2",
    "crdt_causal_consistency": False,
    "authentication_sasl_pass": "",
    "import_failure_reason": "",
    "oss_cluster": False,
    "sync": "disabled",
    "background_op": [
      {
        "status": "idle"
      }
    ],
    "authentication_ssl_crdt_certs": [],
    "port": 14092,
    "crdt_guid": "",
    "version": "6.0.4",
    "email_alerts": False,
    "max_aof_load_time": 3600,
    "crdt_sources": [],
    "auto_upgrade": False,
    "backup_interval": 86400,
    "slave_ha_priority": 0,
    "shards_placement": "dense",
    "data_persistence": "disabled",
    "crdt_sync": "disabled",
    "backup_status": "",
    "crdt": False,
    "crdt_replicas": "",
    "snapshot_policy": [],
    "backup": False,
    "gradual_sync_max_shards_per_source": 1,
    "backup_interval_offset": 0,
    "tls_mode": "disabled",
    "replica_sync": "disabled",
    "authentication_redis_pass": "",
    "implicit_shard_key": False,
    "max_aof_file_size": 322122547200,
    "bigstore": False,
    "max_connections": 0,
    "module_list": [
      {
        "module_name": "rg",
        "module_id": "984757126a4d53a6779bfee6095564cb",
        "semantic_version": "1.0.1",
        "module_args": "cassandra_pass cassandra CreateVenv 1 cassandra_host cassandra"
      }
    ],
    "eviction_policy": "volatile-lru",
    "type": "redis",
    "backup_history": 0,
    "sync_sources": [],
    "crdt_ghost_replica_ids": "",
    "replica_sources": [],
    "shard_block_foreign_keys": True,
    "enforce_client_authentication": "enabled",
    "crdt_replica_id": 0,
    "crdt_config_version": 0,
    "proxy_policy": "single",
    "aof_policy": "appendfsync-every-sec",
    "endpoints": [
      {
        "oss_cluster_api_preferred_ip_type": "internal",
        "uid": "1:1",
        "dns_name": "redis-14092.re-cluster1.ps-redislabs.org",
        "addr_type": "external",
        "proxy_policy": "single",
        "port": 14092,
        "addr": [
          "172.16.22.21"
        ]
      }
    ],
    "wait_command": True,
    "uid": 1,
    "authentication_sasl_uname": "",
    "backup_failure_reason": "",
    "bigstore_ram_size": 0,
    "shard_block_crossslot_keys": False,
    "acl": [],
    "slave_ha": False,
    "internal": False,
    "shards_count": 2,
    "shard_key_regex": [
      {
        "regex": ".*\\{(?<tag>.*)\\}.*"
      },
      {
        "regex": "(?<tag>.*)"
      }
    ],
    "status": "active",
    "gradual_sync_mode": "auto",
    "mkms": True,
    "gradual_src_max_sources": 1,
    "sharding": True,
    "oss_cluster_api_preferred_ip_type": "internal",
    "ssl": False,
    "dns_address_master": "",
    "import_progress": 0.0
  }
]