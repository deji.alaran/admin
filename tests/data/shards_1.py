shards_1=[
  {
    "status": "active",
    "loading": {
      "status": "idle"
    },
    "bdb_uid": 1,
    "uid": "1",
    "detailed_status": "ok",
    "node_uid": "1",
    "assigned_slots": "0-8191",
    "sync": {
      "status": "idle"
    },
    "report_timestamp": "2020-07-14T22:08:07Z",
    "role": "slave",
    "redis_info": {
      "loading": 0,
      "used_memory": 7694344,
      "master_port": 29365,
      "master_host": "172.16.22.22",
      "used_memory_human": "7.34M",
      "master_link_status": "up",
      "used_memory_rss": 18444288,
      "master_sync_in_progress": 0,
      "role": "slave",
      "rlec_version": "6.0.6-35",
      "rlec_fingerprint": "9c932c3eaf934b5ac545e1a15754514b"
    }
  },
  {
    "status": "active",
    "loading": {
      "status": "idle"
    },
    "bdb_uid": 1,
    "uid": "3",
    "detailed_status": "ok",
    "node_uid": "3",
    "assigned_slots": "0-8191",
    "report_timestamp": "2020-07-14T22:08:10Z",
    "role": "master",
    "redis_info": {
      "loading": 0,
      "used_memory": 8185264,
      "used_memory_human": "7.81M",
      "role": "master",
      "used_memory_rss": 18907136,
      "rlec_version": "6.0.6-35",
      "rlec_fingerprint": "9c932c3eaf934b5ac545e1a15754514b"
    }
  },
  {
    "status": "active",
    "loading": {
      "status": "idle"
    },
    "bdb_uid": 1,
    "uid": "4",
    "detailed_status": "ok",
    "node_uid": "3",
    "assigned_slots": "8192-16383",
    "report_timestamp": "2020-07-14T22:08:08Z",
    "role": "master",
    "redis_info": {
      "loading": 0,
      "used_memory": 8144568,
      "used_memory_human": "7.77M",
      "role": "master",
      "used_memory_rss": 18767872,
      "rlec_version": "6.0.6-35",
      "rlec_fingerprint": "9c932c3eaf934b5ac545e1a15754514b"
    }
  },
  {
    "status": "active",
    "loading": {
      "status": "idle"
    },
    "bdb_uid": 1,
    "uid": "2",
    "detailed_status": "ok",
    "node_uid": "2",
    "assigned_slots": "8192-16383",
    "sync": {
      "status": "idle"
    },
    "report_timestamp": "2020-07-14T22:08:08Z",
    "role": "slave",
    "redis_info": {
      "loading": 0,
      "used_memory": 7736048,
      "master_port": 20344,
      "master_host": "172.16.22.22",
      "used_memory_human": "7.38M",
      "master_link_status": "up",
      "used_memory_rss": 17879040,
      "master_sync_in_progress": 0,
      "role": "slave",
      "rlec_version": "6.0.6-35",
      "rlec_fingerprint": "9c932c3eaf934b5ac545e1a15754514b"
    }
  }
]