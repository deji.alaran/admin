shards = [
  {
    "status": "active",
    "loading": {
      "status": "idle"
    },
    "bdb_uid": 1,
    "uid": "1",
    "detailed_status": "ok",
    "node_uid": "1",
    "assigned_slots": "0-16383",
    "report_timestamp": "2020-06-21T16:13:18Z",
    "role": "master",
    "redis_info": {
      "loading": 0,
      "used_memory": 3003464,
      "used_memory_human": "2.86M",
      "role": "master",
      "used_memory_rss": 5451776,
      "rlec_version": "5.4.14-28",
      "rlec_fingerprint": "92b23039a6b28568ef97548ca1a72c8d"
    }
  },
  {
    "status": "active",
    "loading": {
      "status": "idle"
    },
    "bdb_uid": 1,
    "uid": "2",
    "detailed_status": "ok",
    "node_uid": "2",
    "assigned_slots": "0-16383",
    "sync": {
      "status": "idle"
    },
    "report_timestamp": "2020-06-21T16:13:18Z",
    "role": "slave",
    "redis_info": {
      "loading": 0,
      "used_memory": 3003152,
      "master_port": 20389,
      "master_host": "172.16.22.21",
      "used_memory_human": "2.86M",
      "master_link_status": "up",
      "used_memory_rss": 5386240,
      "master_sync_in_progress": 0,
      "role": "slave",
      "rlec_version": "5.4.14-28",
      "rlec_fingerprint": "92b23039a6b28568ef97548ca1a72c8d"
    }
  }
]