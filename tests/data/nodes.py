nodes = [
  {
    "total_memory": 15767126016,
    "uid": 1,
    "bigredis_storage_path": "/var/opt/redislabs/flash",
    "accept_servers": True,
    "shard_list": [
      1
    ],
    "uptime": 1709,
    "addr": "172.16.22.21",
    "ephemeral_storage_size": 154089091072.0,
    "persistent_storage_path": "/var/opt/redislabs/persist",
    "os_version": "Ubuntu 18.04.4 LTS",
    "supported_database_versions": [
      {
        "db_type": "memcached",
        "version": "1.4.17"
      },
      {
        "db_type": "redis",
        "version": "6.0.4"
      },
      {
        "db_type": "redis",
        "version": "5.0.9"
      }
    ],
    "rack_id": "",
    "max_listeners": 100,
    "status": "active",
    "os_name": "ubuntu",
    "software_version": "6.0.6-35",
    "ephemeral_storage_path": "/var/opt/redislabs/tmp",
    "external_addr": [],
    "bigstore_driver": "",
    "os_semantic_version": "18.04",
    "persistent_storage_size": 154089091072.0,
    "architecture": "x86_64",
    "shard_count": 1,
    "cores": 4,
    "max_redis_servers": 100
  },
  {
    "total_memory": 15767126016,
    "uid": 3,
    "bigredis_storage_path": "/var/opt/redislabs/flash",
    "accept_servers": True,
    "shard_list": [
      3,
      4
    ],
    "uptime": 1709,
    "addr": "172.16.22.22",
    "ephemeral_storage_size": 154089091072.0,
    "persistent_storage_path": "/var/opt/redislabs/persist",
    "os_version": "Ubuntu 18.04.4 LTS",
    "supported_database_versions": [
      {
        "db_type": "memcached",
        "version": "1.4.17"
      },
      {
        "db_type": "redis",
        "version": "6.0.4"
      },
      {
        "db_type": "redis",
        "version": "5.0.9"
      }
    ],
    "rack_id": "",
    "max_listeners": 100,
    "status": "active",
    "os_name": "ubuntu",
    "software_version": "6.0.6-35",
    "ephemeral_storage_path": "/var/opt/redislabs/tmp",
    "external_addr": [],
    "os_semantic_version": "18.04",
    "persistent_storage_size": 154089091072.0,
    "architecture": "x86_64",
    "shard_count": 2,
    "cores": 4,
    "max_redis_servers": 100
  },
  {
    "total_memory": 15767126016,
    "uid": 2,
    "bigredis_storage_path": "/var/opt/redislabs/flash",
    "accept_servers": True,
    "shard_list": [
      2
    ],
    "uptime": 1709,
    "addr": "172.16.22.23",
    "ephemeral_storage_size": 154089091072.0,
    "persistent_storage_path": "/var/opt/redislabs/persist",
    "os_version": "Ubuntu 18.04.4 LTS",
    "supported_database_versions": [
      {
        "db_type": "memcached",
        "version": "1.4.17"
      },
      {
        "db_type": "redis",
        "version": "6.0.4"
      },
      {
        "db_type": "redis",
        "version": "5.0.9"
      }
    ],
    "rack_id": "",
    "max_listeners": 100,
    "status": "active",
    "os_name": "ubuntu",
    "software_version": "6.0.6-35",
    "ephemeral_storage_path": "/var/opt/redislabs/tmp",
    "external_addr": [],
    "os_semantic_version": "18.04",
    "persistent_storage_size": 154089091072.0,
    "architecture": "x86_64",
    "shard_count": 1,
    "cores": 4,
    "max_redis_servers": 100
  }
]